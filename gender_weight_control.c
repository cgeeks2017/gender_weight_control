#include <stdio.h>

/*
 *   Swap_gender
 *   This function will decide if is a female or a male accordindly to the gender initials provides by the user.
 *
 *   @param char gender
 *     A weight as integer in Kg that the user provides.

 *   @param int weight
 *     A weight in Kg provides by the user.
 *
 */
void swap_gender(char gender, int weight) {
  switch (gender) {
    case 'f':
    case 'F':
      printf("\t You are female! \n");
      break;

    case 'm':
    case 'M':
      printf("\t You are male! \n");
      break;

    default:
      break;
  }
}

/*
 *   Monitoring weight.
 *   Base on the weight the monitor will display a warning message to the user.
 *
 *   @param int weight
 *     A weight as integer in Kg that the user provides.
 *
 */
void monitoring_weight_female(int weight) {
  if (weight <= 50) {
    printf("\t Weight less than or equal to 50(Kg) => %d(Kg) \n", weight);
  }
  else if (weight > 50 && weight <= 80) {
    printf("\t You should be aware that you are weighing less than or equal to 80(Kg) => %d(Kg) \n", weight);
  }
  else {
    printf("\t You should be aware that you are weighing more than 80(Kg) => %d(Kg) \n", weight);
  }
}

/*
 * Auxiliary function just to test and debug the function swap_gender.
 */
void test_swap_gender() {
  swap_gender('f', 50);
  swap_gender('M', 60);
  swap_gender('m', 90);
  swap_gender('F', 20);
}

int main() {
  // Here I am testing the monitoring_weight_female function.
  monitoring_weight_female(10);
  monitoring_weight_female(50);
  monitoring_weight_female(80);
  monitoring_weight_female(0);

  return 0;
}